package com.percivalruiz.weatherapp.Activity;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.percivalruiz.weatherapp.Fragment.RefreshFragment;
import com.percivalruiz.weatherapp.Fragment.WeatherListFragment;
import com.percivalruiz.weatherapp.Model.DataList;
import com.percivalruiz.weatherapp.Network.ApiInterface;
import com.percivalruiz.weatherapp.R;
import com.percivalruiz.weatherapp.WeatherAppApplication;
import com.percivalruiz.weatherapp.databinding.ActivityMainBinding;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements RefreshFragment.RefreshList {

  private CompositeDisposable mCompositeDisposable;
  @Inject ApiInterface apiInterface;
  private static String CITIES = "2643743,3067696,3837675";
  private ActivityMainBinding binding;
  private ProgressDialog progressDialog;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
    ((WeatherAppApplication) getApplication()).getAppComponent().inject(MainActivity.this);
    mCompositeDisposable = new CompositeDisposable();
    progressDialog = new ProgressDialog(this);
    progressDialog.setMessage("Loading");

    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

    Fragment refreshFragment = new RefreshFragment();
    fragmentTransaction.add(R.id.refresh_fragment_container, refreshFragment);

    Fragment weatherFragment = WeatherListFragment.newInstance(null);
    fragmentTransaction.add(R.id.fragment_container, weatherFragment);
    fragmentTransaction.commit();

    getWeather(CITIES);
  }

  private void createFragmentView(DataList dataList) {
    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

    Fragment fragment = WeatherListFragment.newInstance(dataList);
    fragmentTransaction.replace(R.id.fragment_container, fragment);
    fragmentTransaction.commit();
  }

  private void getWeather(String cities) {
    mCompositeDisposable.add(apiInterface.getWeather(cities)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .doOnSubscribe(
            __ -> progressDialog.show()
        )
        .subscribe(response -> {
          Log.d("test", response.getCnt() + " ");
          createFragmentView(response);
        }, error -> {
          Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
          Log.e("test", "Error fetching data" + error.getMessage());
          progressDialog.hide();
        }, () -> progressDialog.dismiss()));
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    mCompositeDisposable.clear();
  }

  @Override public void onRefresh() {
    getWeather(CITIES);
  }
}
