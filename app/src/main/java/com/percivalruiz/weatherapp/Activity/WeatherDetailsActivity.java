package com.percivalruiz.weatherapp.Activity;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.percivalruiz.weatherapp.Fragment.RefreshFragment;
import com.percivalruiz.weatherapp.Fragment.WeatherListFragment;
import com.percivalruiz.weatherapp.Model.Data;
import com.percivalruiz.weatherapp.Model.DataList;
import com.percivalruiz.weatherapp.Model.Weather;
import com.percivalruiz.weatherapp.Network.ApiInterface;
import com.percivalruiz.weatherapp.R;
import com.percivalruiz.weatherapp.WeatherAppApplication;
import com.percivalruiz.weatherapp.databinding.ActivityMainBinding;
import com.percivalruiz.weatherapp.databinding.ActivityWeatherDetailsBinding;
import com.percivalruiz.weatherapp.databinding.WeatherItemImageLayoutBinding;
import com.percivalruiz.weatherapp.databinding.WeatherItemLayoutBinding;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class WeatherDetailsActivity extends AppCompatActivity {

  private CompositeDisposable mCompositeDisposable;
  @Inject ApiInterface apiInterface;
  private static String CITIES = "2643743,3067696,3837675";
  private ActivityWeatherDetailsBinding binding;
  private ProgressDialog progressDialog;
  private Data data;
  private WeatherAdapter weatherAdapter;
  private List<Weather> weatherList;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.setContentView(this, R.layout.activity_weather_details);
    ((WeatherAppApplication) getApplication()).getAppComponent()
        .inject(WeatherDetailsActivity.this);
    mCompositeDisposable = new CompositeDisposable();
    setSupportActionBar(binding.toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    progressDialog = new ProgressDialog(this);
    progressDialog.setMessage("Loading");

    data = (Data) getIntent().getSerializableExtra(WeatherListFragment.DATA);
    binding.setData(data);
    weatherList = new ArrayList<>();
    weatherList.addAll(data.getWeather());

    binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
    weatherAdapter = new WeatherAdapter(weatherList);
    binding.recyclerView.setAdapter(weatherAdapter);

    binding.refreshBtn.setOnClickListener(view -> {
      getWeather();
    });
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    if (id == android.R.id.home) {
      onBackPressed();
    }

    return super.onOptionsItemSelected(item);
  }

  private class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> {

    private List<Weather> weatherList;

    public WeatherAdapter(List<Weather> weatherList) {
      this.weatherList = weatherList;
    }

    @Override public WeatherAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      WeatherItemImageLayoutBinding binding =
          DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
              R.layout.weather_item_image_layout, parent, false);

      WeatherAdapter.ViewHolder viewHolder = new WeatherAdapter.ViewHolder(binding);
      return viewHolder;
    }

    @Override public void onBindViewHolder(WeatherAdapter.ViewHolder holder, int position) {
      Weather weather = weatherList.get(position);
      holder.binding.setWeather(weather);
      Glide.with(WeatherDetailsActivity.this)
          .load("http://openweathermap.org/img/w/" + weather.getIcon() + ".png")
          .into(holder.binding.icon);
    }

    @Override public int getItemCount() {
      return weatherList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
      public WeatherItemImageLayoutBinding binding;

      public ViewHolder(WeatherItemImageLayoutBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
      }
    }
  }

  private void getWeather() {
    mCompositeDisposable.add(apiInterface.getData(String.valueOf(data.getId()))
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .doOnSubscribe(__ -> progressDialog.show())
        .subscribe(response -> {
          Log.d("test", response.getName() + " ");
          data = response;
          binding.setData(data);
          weatherList.clear();
          weatherList.addAll(data.getWeather());
          weatherAdapter.notifyDataSetChanged();
        }, error -> {
          Toast.makeText(WeatherDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT)
              .show();
          Log.e("test", "Error fetching data" + error.getMessage());
          progressDialog.hide();
        }, () -> progressDialog.dismiss()));
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    mCompositeDisposable.clear();
  }
}
