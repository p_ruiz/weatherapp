package com.percivalruiz.weatherapp;

import android.app.Application;
import com.percivalruiz.weatherapp.DI.AppComponent;
import com.percivalruiz.weatherapp.DI.AppModule;
import com.percivalruiz.weatherapp.DI.DaggerAppComponent;

public class WeatherAppApplication extends Application {

  private AppComponent appComponent;

  @Override public void onCreate() {
    super.onCreate();

    this.appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();

  }

  public AppComponent getAppComponent() {
    return appComponent;
  }

}
