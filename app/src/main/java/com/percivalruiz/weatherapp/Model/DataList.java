package com.percivalruiz.weatherapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

public class DataList implements Serializable{
  @SerializedName("cnt")
  @Expose
  private Integer cnt;
  @SerializedName("list")
  @Expose
  private List<Data> list = null;

  public Integer getCnt() {
    return cnt;
  }

  public void setCnt(Integer cnt) {
    this.cnt = cnt;
  }

  public java.util.List<Data> getList() {
    return list;
  }

  public void setList(java.util.List<Data> list) {
    this.list = list;
  }
}
