package com.percivalruiz.weatherapp.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.percivalruiz.weatherapp.databinding.FragmentRefreshBinding;

public class RefreshFragment extends Fragment {

  public interface RefreshList {
    public void onRefresh();
  }

  private RefreshList refreshList;

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    FragmentRefreshBinding binding =
        FragmentRefreshBinding.inflate(inflater, container, false);
    binding.refreshBtn.setOnClickListener(view -> {
      refreshList.onRefresh();
    });
    return binding.getRoot();
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    refreshList = (RefreshList) context;

  }
}
