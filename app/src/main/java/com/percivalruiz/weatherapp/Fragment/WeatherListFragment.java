package com.percivalruiz.weatherapp.Fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.percivalruiz.weatherapp.Activity.WeatherDetailsActivity;
import com.percivalruiz.weatherapp.Model.Data;
import com.percivalruiz.weatherapp.Model.DataList;
import com.percivalruiz.weatherapp.Model.Weather;
import com.percivalruiz.weatherapp.R;
import com.percivalruiz.weatherapp.databinding.FragmentWeatherListBinding;
import com.percivalruiz.weatherapp.databinding.WeatherDataItemLayoutBinding;
import com.percivalruiz.weatherapp.databinding.WeatherItemLayoutBinding;
import java.util.List;

public class WeatherListFragment extends Fragment {

  private static String DATA_LIST = "dataList";
  public static String DATA = "data";
  private DataList dataList;
  private Adapter adapter;

  private TextView mNameTextView;

  public static WeatherListFragment newInstance(DataList dataList) {
    Bundle bundle = new Bundle();
    bundle.putSerializable(DATA_LIST, dataList);

    WeatherListFragment fragment = new WeatherListFragment();
    fragment.setArguments(bundle);

    return fragment;
  }

  private void readBundle(Bundle bundle) {
    if (bundle != null) {
      dataList = (DataList) bundle.getSerializable(DATA_LIST);
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    FragmentWeatherListBinding binding =
        FragmentWeatherListBinding.inflate(inflater, container, false);
    readBundle(getArguments());

    if(dataList != null) {
      binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
      adapter = new Adapter(dataList.getList());
      binding.recyclerView.setAdapter(adapter);
    }
    return binding.getRoot();
  }

  public interface WeatherEventsListener {
    public void viewWeatherDetails(Data data);
  }

  private class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> {

    private List<Weather> weatherList;

    public WeatherAdapter(List<Weather> weatherList) {
      this.weatherList = weatherList;
    }

    @Override public WeatherAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      WeatherItemLayoutBinding binding =
          DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
              R.layout.weather_item_layout, parent, false);

      ViewHolder viewHolder = new ViewHolder(binding);
      return viewHolder;
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
      Weather weather = weatherList.get(position);
      holder.binding.setWeather(weather);
    }

    @Override public int getItemCount() {
      return weatherList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
      public WeatherItemLayoutBinding binding;

      public ViewHolder(WeatherItemLayoutBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
      }
    }
  }

  public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder>
      implements WeatherEventsListener {

    private List<Data> dataList;
    private WeatherAdapter weatherAdapter;

    public Adapter(List<Data> dataList) {
      this.dataList = dataList;
    }

    @Override public Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      WeatherDataItemLayoutBinding binding =
          DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
              R.layout.weather_data_item_layout, parent, false);

      ViewHolder viewHolder = new ViewHolder(binding);
      return viewHolder;
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
      Data data = dataList.get(position);
      holder.binding.setData(data);
      holder.binding.setItemClickListener(this);
      holder.binding.weatherRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
      weatherAdapter = new WeatherAdapter(data.getWeather());
      holder.binding.weatherRecyclerView.setAdapter(weatherAdapter);
    }

    @Override public int getItemCount() {
      return dataList.size();
    }

    @Override public void viewWeatherDetails(Data data) {
      Intent intent = new Intent(getActivity(), WeatherDetailsActivity.class);
      intent.putExtra(DATA, data);
      startActivity(intent);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
      public WeatherDataItemLayoutBinding binding;

      public ViewHolder(WeatherDataItemLayoutBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
      }
    }
  }
}
