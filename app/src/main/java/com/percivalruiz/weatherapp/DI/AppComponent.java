package com.percivalruiz.weatherapp.DI;

import com.percivalruiz.weatherapp.Activity.MainActivity;
import com.percivalruiz.weatherapp.Activity.WeatherDetailsActivity;
import com.percivalruiz.weatherapp.Network.ApiInterface;
import dagger.Component;
import javax.inject.Singleton;

@Singleton @Component(modules = AppModule.class)
public interface AppComponent {

  void inject(MainActivity mainActivity);

  void inject(WeatherDetailsActivity weatherDetailsActivity);

  ApiInterface apiInterface();

}
