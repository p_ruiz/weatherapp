package com.percivalruiz.weatherapp.Network;

import com.percivalruiz.weatherapp.Model.Data;
import com.percivalruiz.weatherapp.Model.DataList;
import io.reactivex.Observable;
import java.util.List;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

  @GET("data/2.5/weather?APPID=e5436fa538b33909f9c5a0bcd660596a&units=metric")
  Observable<Data> getData(@Query("id") String city);

  @GET("data/2.5/group?APPID=e5436fa538b33909f9c5a0bcd660596a&units=metric")
  Observable<DataList> getWeather(@Query("id") String city);
}
