package com.percivalruiz.weatherapp.Custom;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomConstraintLayout extends ConstraintLayout {
  public CustomConstraintLayout(Context context) {
    super(context);
  }

  public CustomConstraintLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public CustomConstraintLayout(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override public boolean onInterceptTouchEvent(MotionEvent e) {
    return true;
  }
}
